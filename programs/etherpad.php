<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';


require_once dirname(__FILE__) . '/EtherpadLite/etherpad-lite-client.php';

require_once dirname(__FILE__) . '/functions.php';
require_once dirname(__FILE__) . '/pad.class.php';



/**
 * This functionality provides the possibility to manage pads on
 * an etherpad server.
 */
class Func_Etherpad extends bab_Functionality
{
    /**
     * @var EtherpadLiteClient $client
     */
    private $_client = null;


    /**
     *
     * @param string $apiKey
     * @param string $baseUrl
     * @return EtherpadLiteClient
     */
    public function init($apiKey = null, $baseUrl = null)
    {
        if (!isset($apiKey)) {
            $apiKey = LibEtherpad_getConfiguration('apiKey', '');
        }
        if (!isset($baseUrl)) {
            $baseUrl = LibEtherpad_getConfiguration('baseUrl', '');
        }
        $this->_client = new EtherpadLiteClient($apiKey, $baseUrl . '/api');
    }



    /**
     *
     * @return EtherpadLiteClient
     */
    public function getEtherpadClient()
    {
        if (!isset($this->_client)) {
            $this->init();
        }
        return $this->_client;
    }




    /**
     * Returns a unique for the current instance of ovidentia.
     * @return string
     */
    public function getOvidentiaInstanceId()
    {
        return LibEtherpad_getConfiguration('ovidentiaInstanceId', null);
    }



    /**
     * Creates a new pad associated to the reference.
     *
     * @param string $padName
     * @param string $reference
     *
     * @return LibEtherpad_Pad
     */
    public function createPad($padName, $reference = null)
    {
        $padSet = $this->PadSet();
        $pads = $padSet->select(
            $padSet->name->is($padName)->_AND_($padSet->reference->is($reference))
        );

        if ($pads->count() > 0) {
            throw new LibEtherpad_Exception(LibEtherpad_translate('A pad with this name already exists.'));
        }

        // When creating a pad on etherpad, its name is limited in size and
        // some characters are replaced. So we use an md5 of the
        // original name.
        $epPadName = md5($padName);

        $client = $this->getEtherpadClient();

        $fullReference = $reference;
        $ovidentiaInstanceId = $this->getOvidentiaInstanceId();
        if (isset($ovidentiaInstanceId)) {
            $fullReference = $ovidentiaInstanceId . '.' .  $fullReference;
        }


        $result = $client->createGroupIfNotExistsFor($fullReference);
        $epGroupId = $result->groupID;


        $client->createGroupPad($epGroupId, $epPadName, null);

        $epPadId =  $epGroupId . '$' . $epPadName;

        $pad = $padSet->newRecord();
        $pad->name = $padName;
        $pad->reference = $reference;
        $pad->epPadId = $epPadId;

        $pad->save();

        return $pad;
    }




    /**
     *
     * @return LibEtherpad_PadSet
     */
    public function PadSet()
    {
        $padSet = new LibEtherpad_PadSet();
        $padSet->setEtherpad($this);
        return $padSet;
    }

    /**
     *
     * @param string $padId
     * @return LibEtherpad_Pad
     */
    public function getPad($padId)
    {
        $padSet = $this->PadSet();
        $pad = $padSet->get($padId);

        return $pad;
    }
}
