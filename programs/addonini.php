;<?php/*

[general]
name="LibEtherpad"
version="0.1.4"
description="Library providing access to Etherpad Lite servers"
description.fr="Librairie d'accès aux serveurs Etherpad Lite"
encoding="UTF-8"
delete=1
ov_version="8.1.0"
php_version="5.1"
mysql_character_set_database="latin1,utf8"
addon_access_control="0"
author="Laurent Choulette (laurent.choulette@cantico.fr)"
icon="etherpad.png"

;*/?>
