<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';


require_once dirname(__FILE__) . '/EtherpadLite/etherpad-lite-client.php';

require_once dirname(__FILE__) . '/functions.php';





// Initialize mysql ORM backend.
bab_functionality::get('LibOrm')->initMySql();
ORM_MySqlRecordSet::setBackend(new ORM_MySqlBackend($GLOBALS['babDB']));

/**
 *
 * @property ORM_PkField          $id
 * @property ORM_StringField      $epPadId
 * @property ORM_StringField      $name
 * @property ORM_StringField      $reference
 */
class LibEtherpad_PadSet extends ORM_MySqlRecordSet
{
    /**
     * @var Func_Etherpad $_etherpad
     */
    private $_etherpad = null;


    public function __construct()
    {
        parent::__construct();

        $this->setPrimaryKey('id');


        $this->addFields(
            ORM_StringField('epPadId')
                ->setDescription('The etherpad pad id'),
            ORM_StringField('name')
                ->setDescription('The human readable name'),
            ORM_StringField('reference')
                ->setDescription('A reference of the ovidentia object this pad is linked to')
        );

    }


    /**
     *
     * @param Func_Etherpad $etherpad
     */
    public function setEtherpad(Func_Etherpad $etherpad)
    {
        $this->_etherpad = $etherpad;
        return $this;
    }

    /**
     * @return Func_Etherpad
     */
    public function getEtherpad()
    {
        return $this->_etherpad;
    }

    /**
     * @return EtherpadLiteClient
     */
    public function getEtherpadClient()
    {
        return $this->_etherpad->getEtherpadClient();
    }


    /**
     * (non-PHPdoc)
     * @see ORM_RecordSet::delete()
     */
    public function delete(ORM_Criteria $oCriteria = null)
    {
        $etherpadClient = $this->getEtherpadClient();
        $records = $this->select($oCriteria);
        foreach ($records as $record) {
            try {
                $etherpadClient->deletePad($record->epPadId);
            } catch (Exception $e) {

            }
            parent::delete($this->id->is($record->id));
        }

        return true;
    }
}




/**
 *
 * @property int        $id
 * @property string     $epPadId
 * @property string     $name
 * @property string     $reference
 */
class LibEtherpad_Pad extends ORM_MySqlRecord
{


    /**
     * @var bool $_showChat
     */
    private $_showChat = false;


    /**
     * @var bool $_showLineNumbers
     */
    private $_showLineNumbers = false;




    /**
     * @return EtherpadLiteClient
     */
    private function _etherpadClient()
    {
        return $this->getParentSet()->getEtherpadClient();
    }

    /**
     * @return Func_Etherpad
     */
    private function _etherpad()
    {
        return $this->getParentSet()->getEtherpad();
    }



    /**
     * Returns the number of revisions of this pad.
     * @return int
     */
    public function getRevisionsCount()
    {
        $result = $this->_etherpadClient()->getRevisionsCount($this->epPadId);
        return $result->revisions;
    }



    /**
     * Returns the time the pad was last edited as a Unix timestamp.
     * @return int unix timestamp.
     */
    public function getLastEdited()
    {
        $result = $this->_etherpadClient()->getLastEdited($this->epPadId);
        return $result->lastEdited / 1000;
    }


    /**
     * Returns the html content of the pad for the specified revision.
     *
     * @param string $rev
     * @return string
     */
    public function getHtml($rev = null)
    {
        $this->archiveContent();
        $result = $this->_etherpadClient()->getHTML($this->epPadId, $rev);
        return $result->html;
    }


    /**
     * Sets the text of the pad based on HTML, HTML must be well formed.
     * Malformed HTML will send a warning to the API log.
     * @param string $html
     */
    public function setHTML($html)
    {
        return $this->_etherpadClient()->setHTML($this->epPadId, $html);
    }


    /**
     * Returns the text of a pad.
     * Example returns:
     * {code: 0, message:"ok", data: {text:"Welcome Text"}}
     * {code: 1, message:"padID does not exist", data: null}
     * @param string $padId
     * @param string $rev
     */
    public function getText($rev = null)
    {
        $this->archiveContent();
        $result = $this->_etherpadClient()->getText($this->epPadId, $rev);
        return bab_getStringAccordingToDataBase($result->text, bab_charset::UTF_8) ;
    }


    /**
     * Sets the text of the pad.
     * @param string $text
     */
    public function setText($text)
    {
        return $this->_etherpadClient()->setText($this->epPadId, bab_convertStringFromDatabase($text, bab_charset::UTF_8));
    }



    /**
     *
     * @param string $rev
     */
    public function archiveContent($rev = null)
    {
        if (!isset($rev)) {
            $rev = $this->getRevisionsCount();
        }
        $result = $this->_etherpadClient()->getHTML($this->epPadId, $rev);

        return LibEtherpad_setConfiguration('pads/' . $this->epPadId . '/archive/' . $rev, $result->html);
    }


    /**
     *
     * @param string $rev
     */
    public function getArchivedRevisions()
    {
        $revs = LibEtherpad_getConfigurationKeys('pads/' . $this->epPadId . '/archive/');

        return $revs;
    }


    /**
     *
     * @param string $rev
     */
    public function getArchivedContent($rev = null)
    {
        return LibEtherpad_getConfiguration('pads/' . $this->epPadId . '/archive/' . $rev);
    }



    /**
     *
     * @param string $sessionId
     * @return string
     */
    public function getIframe($width = '100%', $height = '100%')
    {
        $baseUrl = LibEtherpad_getConfiguration('baseUrl', '');

        $src = $baseUrl . '/p/' . $this->epPadId;
        if ($this->_showChat) {
            $src .= '?showChat=true';
        } else {
            $src .= '?showChat=false';
        }
        if ($this->_showLineNumbers) {
            $src .= '&showLineNumbers=true';
        } else {
            $src .= '&showLineNumbers=false';
        }


        $userId = bab_getUserId();
        $userName = bab_getUserName($userId, true);

        $result = $this->_etherpadClient()->createAuthorIfNotExistsFor($userId, bab_convertStringFromDatabase($userName, bab_charset::UTF_8));
        $epAuthorId = $result->authorID;

        $fullReference = $this->reference;
        $ovidentiaInstanceId = $this->_etherpad()->getOvidentiaInstanceId();
        if (isset($ovidentiaInstanceId)) {
            $fullReference = $ovidentiaInstanceId . '.' .  $fullReference;
        }

        $result = $this->_etherpadClient()->createGroupIfNotExistsFor($fullReference);
        $epGroupId = $result->groupID;

        $sessionEndTimestamp = time() + (7 * 24 * 60 * 60);
        
        $result = $this->_etherpadClient()->createSession($epGroupId, $epAuthorId, $sessionEndTimestamp);
        $sessionId =  $result->sessionID;

        if (isset($sessionId)) {
            setcookie('sessionID', $sessionId, $sessionEndTimestamp, '/');
            $domain = substr($baseUrl, 0, -5);

            $src = $domain . '/setsession.html?sessionID=' . $sessionId . '&amp;redirect=' . $src;
        }

        $iframe = '<iframe style="border: 1px solid rgba(0, 0, 0, 0.1)" src="' . $src . '" width="' . $width . '" height="' . $height . '"></iframe>';

        return $iframe;
    }




    /**
     * Deletes the pad.
     */
    public function delete()
    {
        $set = $this->getParentSet();
        $set->delete($set->id->is($this->id));
    }
}
