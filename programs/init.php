<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2014 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';

require_once dirname(__FILE__) . '/functions.php';




function LibEtherpad_upgrade($version_base, $version_ini)
{
    require_once $GLOBALS['babInstallPath'] .'utilit/functionalityincl.php';
    $functionalities = new bab_functionalities();

    $addonName = 'LibEtherpad';
    $addonInfo = bab_getAddonInfosInstance($addonName);

    $addonPhpPath = $addonInfo->getPhpPath();


    if (false === $functionalities->register('etherpad', $addonPhpPath . 'etherpad.php')) {
        return false;
    }

    LibEtherpad_getConfiguration('apiKey', '');
    LibEtherpad_getConfiguration('baseUrl', '');
    LibEtherpad_getConfiguration('ovidentiaInstanceId', uniqid());


    // create tables
    require_once $GLOBALS['babInstallPath'] . 'utilit/devtools.php';
    require_once dirname(__FILE__) . '/pad.class.php';

    $mysqlbackend = new ORM_MySqlBackend($GLOBALS['babDB']);

    $sql = new bab_synchronizeSql();

    $queries = $mysqlbackend->setToSql(new LibEtherpad_PadSet());
    $sql->fromSqlString($queries);

    return true;
}





function LibEtherpad_onDeleteAddon()
{
    require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';
    $functionalities = new bab_functionalities();

    if (false === $functionalities->unregister('etherpad')) {
        return false;
    }

    return true;
}
